from django.db import models

# Create your models here.

class Membership(models.Model):
    name = models.CharField(max_length=100)
    email = models.EmailField(max_length=100)
    contact = models.CharField(max_length=10)
    age = models.CharField(max_length=2)
    gender = models.CharField(max_length=50, default="")
    plan = models.CharField(max_length=50)
    joinDate = models.DateField(max_length=50)
    expireDate = models.DateField(max_length=50)

    def __str__(self):
        return self.name

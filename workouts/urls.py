from django.urls import path
from workouts.views import home, membership_view

urlpatterns = [
    path("", home, name="home"),
    path("membership/", membership_view, name="membership")
]

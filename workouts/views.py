from django.shortcuts import render, redirect
from workouts.models import Membership
from django.contrib.auth.decorators import login_required
from workouts.forms import MembershipForm
# Create your views here.

def home(request):
    return render(request, "index.html")

@login_required
def membership_view(request):
    membership = Membership.objects.all()
    context = {
        "membership": membership
    }
    return render(request, "membership.html", context)

@login_required
def create_membership(request):
    if request.method == "POST":
        form = MembershipForm(request.POST)
        if form.is_valid():
            membership = form.save(False)
            membership.name = request.user
            membership.save()
            return redirect("membership")
        else:
            form = MembershipForm(request.POST)
        context = {
            "form": form
        }
        return render(request, "create.html", context)
